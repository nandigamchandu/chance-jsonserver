# Json-Server and Chance

[reference for json-server](https://github.com/typicode/json-server#articles)

### configurations
set configuration in `json-server.json`
### To start json-server
```bash
npm install json-server -g
json-server json/db.json
```
### 
[reference for chance.js](https://chancejs.com/index.html)

```bash
yarn add chance
```