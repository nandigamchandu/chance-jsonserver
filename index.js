const jsonServer = require("json-server");
const chance = require("chance").Chance();
const server = jsonServer.create();

// Add routes
const router = jsonServer.router("./json/db.json");

// Add custom routes before JSON Server router

server.get("/chance/basics", (req, res) => {
  res.json({
    boolean: chance.bool(),
    boolean_like_hood_30: chance.bool({ likelihood: 30 }),
    symbol: chance.character({ symbols: true }),
    integer: chance.integer({ min: -20, max: 20 }).toString(),
    string: chance.string({ length: 5, pool: "abcde" }),
    paragraph: chance.paragraph({ sentences: 3 }),
    letter_lower: chance.letter({ casing: "lower" }),
    letter_upper: chance.letter({ casing: "upper" })
  });
});

server.get("/chance/text", (req, res) => {
  res.json({
    sentences: chance.sentence({ words: 5 }),
    paragraph: chance.paragraph({ sentences: 3 }),
    word_three_syllables: chance.word({ syllables: 3 })
  });
});

server.get("/chance/person", (req, res) => {
  res.json({
    first: chance.first(),
    gender: chance.gender(),
    age: chance.age(),
    child_age: chance.age({ type: "child" }),
    birthday: chance.birthday({ american: false, string: true }),
    father_birthday: chance.birthday({
      year: chance.year({ min: 1969, max: 1979 })
    }),
    profession: chance.profession(),
    company: chance.company(),
    email: chance.email({ domain: "gmail.com" }),
    address: chance.address(),
    date_of_join: chance.date(),
    phone: chance.phone({ country: "us", mobile: true }),
    friends: chance.n(chance.twitter, 5),
    pet: chance.animal({ type: "pet" })
  });
});

server.use(router);
server.listen(9000, () => {
  console.log("server started at localhost 9000");
});
